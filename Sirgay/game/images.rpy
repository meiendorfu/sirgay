image bg room:
    "room.jpg"

image bg toire one:
    "toire1.jpg"
    
image bg toire two = "toire2.jpg"

image dvoika one:
    ypos 0.9
    xzoom 1.3 yzoom 1.3
    "dvoika1.png"

image dvoika two:
    ypos 0.9
    xzoom 1.3 yzoom 1.3
    "dvoika2.png"

image talking:
    "g1.png"
    0.1
    xalign 0.9
    0.1
    "g2.png"
    linear 0.5 xalign 0.5
    0.1
    repeat

image sir fedora = "sir_fedora.png"

image side author_side:
    size(185, 182)
    "author.png"
image side sir_side nonson:
    size(185, 182)
    "son2.png"
image side sir_side son:
    size(185, 182)
    "son.png"
image side sir_side toire_static:
    size(185, 182)
    "son3.png"