﻿
define a = Character('', image='author_side') 

label start:
    scene bg room

    #show sir fedora
    $ renpy.music.play('audio/main.mp3', loop=True) 
    $ renpy.music.set_volume(0.01, 0, channel='music')
    
    a "29 апреля, 10 утра."
    a "По всему миру прошёлся смертельный вирус, но на улице флорилор 8/2 всё было тихо и спокойно."
    a "На нижнем этаже двухъярусной кровати гордо и беззаботно посапывал борадатый мальчик."
    a "Но сегодня тот день, когда он станет мужчиной!"
    a "Или просто немного взрослее, потому что у него день рождения..."
    a "В это утро ему снились самые приятные вещи, которые он только мог себе представить."
    show dvoika one with dissolve
    $ renpy.music.play('audio/hi.mp3', channel='sound', fadeout=0.5)
    a "Как он получает 10-ки и его хвалят учителя."
    show dvoika two with dissolve 
    $ renpy.music.play('audio/boo.mp3', channel='sound', fadeout=0.5)
    a "А его сверстники - русские свиньи, получают то, что заслужили."
    hide dvoika with dissolve
    s son "Мхгмм... да... десяточки.... вкуснятина....."
    a "Но вот прозвенел будильник и Серёга проснулся."

    s nonson "Какое чудесное утро! Не терпится скорее взяться за уроки и сыграть 42 игры в тривиадора!"
    s "Но прежде нужно покончить с утренним туалетом."

    scene bg toire one with fade:
        zoom 1.1

    a "Cерёжа зашёл в уборную и приблизился к зеркалу."

    scene bg toire one with dissolve:
        zoom 1.5 
        xpos -0.25
    s toire "Черт побери! Ну и я зарос. Пора бы уже побриться. Хотя, с другой стороны, можно и чем-то другим заняться..."

    menu:
        "Что же мне сделать в ванной?"
        
        "Побриться":
            "a"
            #hide sir fedora
        "Помыть жопу":
            "b"
            #hide sir fedora with easeoutright
            
    "Он съебался"

    return
