﻿init python:
  
    # This is set to the name of the character that is speaking, or
    # None if no character is currently speaking.
    speaking = None
  
    # This returns speaking if the character is speaking, and done if the
    # character is not.
    def while_speaking(name, speak_d, done_d, st, at):
        if speaking == name:
            return speak_d, .1
        else:
            return done_d, None
  
    # Curried form of the above.
    curried_while_speaking = renpy.curry(while_speaking)
  
    # Displays speaking when the named character is speaking, and done otherwise.
    def WhileSpeaking(name, speaking_d, done_d=Null()):
        return DynamicDisplayable(curried_while_speaking(name, speaking_d, done_d))
  
    # This callback maintains the speaking variable.
    def speaker_callback(name, event, **kwargs):
        global speaking
       
        if event == "show":
            renpy.music.set_volume(0.08, channel='sound')
            renpy.music.play("audio/talk.mp3", loop=True, channel='sound')
            speaking = name
        elif event == "slow_done":
            renpy.music.stop(fadeout=0.5, channel='sound')
            speaking = None
        elif event == "end":
            renpy.music.stop(fadeout=0.5, channel='sound')
            speaking = None
  
    # Curried form of the same.
    speaker = renpy.curry(speaker_callback)

define s = Character('Серёжа', image="sir_side", color="#0398fc", callback=speaker("sir"))

image sir mouth normal:
    size(60, 60)
    "mouth1.png"
    .09
    "mouth2.png"
    .09
    "mouth3.png"
    .09
    "mouth4.png"
    .09
    repeat

image side sir_side toire = LiveComposite(
    (185, 182),
    (0, 0), "side sir_side toire_static",
    (60, 80), WhileSpeaking("sir", "sir mouth normal", "mouth3.png"),
    )